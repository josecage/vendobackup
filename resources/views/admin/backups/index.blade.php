@extends('layouts.master')

@section('page_link', Request::fullUrl())
@section('page_desc', 'Sites')
@section('title', 'Sites')
@section('body')
    @livewire('dashboard.backups.backup-list')
@endsection
