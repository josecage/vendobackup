@extends('layouts.master')

@section('page_link', Request::fullUrl())
@section('page_desc', 'Sites')
@section('body')
    @livewire('dashboard.sites.site-list')
@endsection
