<?php

namespace App\Console\Commands;

use App\Models\Backup;
use App\Models\Site;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Config;

class BackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:backup {--clean}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup all Sites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->getSiteList() as $site) {
            $this->setupConfigForSite($site);
            $this->callBackupCommand($site);
        }
    }

    /**
     * @return Site[]|Collection
     */
    protected function getSiteList() {
        return Site::where('enabled', true)->get();
    }

    protected function setupConfigForSite($site)
    {
        Config::set('backup.backup.name', $site->domain);

        Config::set('database.connections.mysql.database', $site->database_name);

        Config::set('backup.backup.source.files.include', $site->path);
    }

    protected function callBackupCommand($site)
    {
        $this->call('backup:'.$this->backupType());

        $this->logBackup($this->backupType(), $site);
    }

    protected function backupType(): string
    {
        return $this->option('clean') ? 'clean' : 'run';
    }

    protected function logBackup($type, $site)
    {
        if ($type === 'run') {
            Backup::create([
            'site_id' => $site->id,
            'payload' => 'Bsckup created successfully',
            'run_at' => now()
        ]);
        }
    }
}
