<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Backup extends Model
{
    use HasFactory;

    protected $fillable = [
        'site_id', 'payload',
        'run_at', 'user_id'
    ];

    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class);
    }
}
