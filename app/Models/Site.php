<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $column, $condition)
 */
class Site extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'domain', 'customer',
        'path', 'database_name', 'enabled'
    ];
}
