<?php

namespace App\Http\Livewire\Dashboard\Sites;

use Livewire\Component;

class SiteEdit extends Component
{
    public function render()
    {
        return view('livewire.dashboard.sites.site-edit');
    }
}
