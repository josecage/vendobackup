<?php

namespace App\Http\Livewire\Dashboard\Sites;

use App\Models\Site;
use Livewire\Component;
use Livewire\WithPagination;

class SiteList extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.dashboard.sites.site-list', [
            'sites' => $this->allSites()
        ]);
    }

    protected function allSites() {
        return Site::paginate(20);
    }
}
