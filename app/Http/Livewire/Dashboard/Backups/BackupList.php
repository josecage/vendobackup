<?php

namespace App\Http\Livewire\Dashboard\Backups;

use App\Models\Backup;
use Livewire\Component;

class BackupList extends Component
{
    public function render()
    {
        return view('livewire.dashboard.backups.backup-list', [
            'backups' => $this->allBackups()
        ]);
    }

    protected function allBackups() {
        return Backup::paginate(20);
    }
}
